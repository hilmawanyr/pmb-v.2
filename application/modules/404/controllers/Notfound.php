<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	public function index()
	{
		$this->load->view('404.php');
	}

}

/* End of file Notfound.php */
/* Location: ./application/modules/404/controllers/Notfound.php */
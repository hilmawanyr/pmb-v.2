

<script type="text/javascript">
    $(document).ready(function () {
        $("#S1").hide();
        $("#S2").hide();

        $("#jen").change(function () {
            if ($("#jen").val() == "1"){
                $("#S1").show('slow');
                $("#S2").hide('slow');
            }else{
                $("#S1").hide('slow');
                $("#S2").show('slow');
            }
        });

        });
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               Pembelian Formulir - Booking Formulir
            </div>
            <div class="panel-body">
                <form method="post" action="<?php echo base_url('dashboard/booking_form/save_booking');?>" class="form-horizontal">
                    <fieldset class="last-child">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenjang *</label>
                            <div class="col-sm-10">
                                <select name="jenjang" id="jen" class="form-control m-b" required="">
                                    <option></option>
                                    <option id="ps1" value="1">Sarjana - S1</option>
                                    <option id="ps2" value="2">Pasca Sarjana - S2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Pendaftar *</label>
                            <div class="col-sm-10">
                                <select name="program" class="form-control m-b" required="">
                                    <option></option>
                                    <option value="MB">Mahasiswa Baru</option>
                                    <!-- <option value="RM">Mahasiswa Readmisi</option> -->
                                    <option value="KV">Mahasiswa Konversi</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-2 control-label">Tipe kelas *</label>
                            <div class="col-sm-10">
                                <select name="account" class="form-control m-b">
                                    <option>-- Pilih Kelas Perkuliahan --</option>
                                    <option>Pagi</option>
                                    <option>Sore</option>
                                    <option>Karyawan</option>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Lokasi kampus *</label>
                            <div class="col-sm-10">
                                <select name="kampus" class="form-control m-b" required="">
                                    <option></option>
                                    <option value="jkt">Jakarta</option>
                                    <option value="bks">Bekasi</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <div id="S1">
                        <fieldset>
                            <h3>Pilih Program Studi Pilihan I</h3>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Hukum</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="74201" name="prodi" required> <i></i> Ilmu Hukum </label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Ekonomi</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="61201" name="prodi" required> <i></i> Manajemen </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="62201" name="prodi" required> <i></i> Akuntansi </label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Teknik</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="55201" name="prodi" required> <i></i> Teknik Informatika </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="26201" name="prodi" required> <i></i> Teknik Industri </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="24201" name="prodi" required> <i></i> Teknik Kimia</label></div>
                                        <div class="i-checks"><label> <input type="radio" value="25201" name="prodi" required> <i></i> Teknik Lingkungan</label></div>
                                        <div class="i-checks"><label> <input type="radio" value="32201" name="prodi" required> <i></i> Teknik Perminyakan</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Psikologi</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="73201" name="prodi" required> <i></i> Ilmu Psikologi </label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Komunikasi</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="70201" name="prodi" required> <i></i> Ilmu Komunikasi </label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fakultas Ilmu Pendidikan</label>
                                <div class="col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="i-checks"><label> <input type="radio" value="85202" name="prodi" required> <i></i> Pendidikan Kepelatihan Olahraga </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="86206" name="prodi" required> <i></i> Pendidikan Guru Sekolah Dasar </label></div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <!-- opsi prodi pilihan II -->
                        <fieldset>
                            <?php for ($i=1; $i < 4; $i++) { ?>
                                <h3>Pilih Program Studi <b><i><u>Peminatan <?= $i ?></u></i></b></h3>
                                <small><i>(program studi yang diminati opsi <?= $i ?>)</i></small>
                                <?= opsiProdi($prodi,$i); ?>
                            <?php } ?>
                        </fieldset>
                    </div>
                    <fieldset id="S2">
                        <h3>Pilih Program Studi Pilihan I</h3>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Direktorat Pasca Sarjana</label>
                            <div class="col-sm-10">
                                <div class="col-sm-10">
                                    <div class="i-checks"><label> <input type="radio" value="74101" name="prodi" required> <i></i> Magister Ilmu Hukum </label></div>
                                    <div class="i-checks"><label> <input type="radio" value="61101" name="prodi" required> <i></i> Magister Manajemen </label></div>
                                </div>
                            </div>
                        </div>

                        <h3>Pilih Program Studi Pilihan II</h3>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Direktorat Pasca Sarjana</label>
                            <div class="col-sm-10">
                                <div class="col-sm-10">
                                    <div class="i-checks"><label> <input type="radio" value="74101" name="prodi2" required> <i></i> Magister Ilmu Hukum </label></div>
                                    <div class="i-checks"><label> <input type="radio" value="61101" name="prodi2" required> <i></i> Magister Manajemen </label></div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <!-- <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Promosi</label>
                        <div class="col-lg-3">
                            <input type="text" placeholder="kode Promosi" class="form-control">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-sm btn-primary">Booking Formulir</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
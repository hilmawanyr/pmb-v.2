<?php $usr = $this->session->userdata('sess_login_pmb'); ?>
<?php $keyy = $this->session->userdata('sess_keyorder'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               Metode Pembayaran & Petunjuk
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingOne">
                                 
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                       Petunjuk Pembayaran ADMISI ONLINE UBHARAJAYA melalui ATM BRI
                                    </a>
                             
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        1. Masukkan kartu ATM ke dalam mesin ATM. <br>
                                        2. Untuk setelan bahasa yang akan digunakan, silahkan sesuaikan. Lalu pilih Lanjutkan. <br>
                                        3. Masukkan nomor PIN ATM BRI Anda. <br>
                                        4. Pilih Transaksi Lain. <br>
                                        5. Pilih Transfer. <br>
                                        6. Pilih BRI <br>
                                        7. Masukkan nomor rekening BRI <b><u>0424-01-000-7183-02</u></b> atas nama <b><u>Universitas Bhayangkara Jakarta Raya</u></b><br>
                                        8. Masukkan jumlah uang sesuai jumlah tagihan, pilih benar (pastikan jumlah uangnya benar). <br>
                                        <b><u><i>Jika dalam waktu 2 x 24 jam pembayaran anda belum terkonfirmasi oleh BPAK UBHARAJAYA, mohon datang secara langsung dengan membawa bukti transaksi berupa struk pembayaran atau hubungi nomor telepon berikut +62 21 88955882 atau +62 21 7267655</i></u></b>
                                    </div>
                                </div>
                            </div><!--end panel collapse-->
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                   
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Petunjuk Pembayaran ADMISI ONLINE UBHARAJAYA melalui ATM BERSAMA (Antar Bank)
                                        </a>
                                
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        1. Masukkan kartu ATM anda ke Mesin ATM yang terdapat logo ATM BERSAMA <br>
                                        2. Masukkan Nomor PIN Anda. <br>
                                        3. Pilih <b>Transfer</b> pada Menu ATM <br>
                                        4. Pilih <b>Transfer ke Bank lain</b> <br>
                                        5. Masukkan Kode <b>Bank BRI (002)</b> dan Nomor Rekening Universitas Bhayangkara Jakarta Raya <b>(0424-01-000-7183-02)</b>. <br>
                                        6. Layar ATM akan menampilkan data transaksi yang akan diproses. Pilih YA jika semua data telah benar. <br>
                                        7. Mesin ATM akan mengeluarkan STRUK ATM sebagai bukti transaksi. Simpan struk tersebut dengan baik. <br>
                                        <b><u><i>Jika dalam waktu 2 x 24 jam pembayaran anda belum terkonfirmasi oleh BPAK UBHARAJAYA, mohon datang secara langsung dengan membawa bukti transaksi berupa struk pembayaran atau hubungi nomor telepon berikut +62 21 88955882 atau +62 21 7267655</i></u></b>
                                    </div>
                                </div>
                            </div><!--end panel collapse-->
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                           Syarat dan Ketentuan
                                        </a>
                                   
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Pembayaran dilakukan maksimal 1 x 24 jam dari waktu pemesanan formulir. Apabila melewati batas waktu maka pemesanan dianggap batal. Uang yang telah dibayarkan guna pembelian formulir tidak dapat dikembalikan apabila terjadi pembatalan pemesanan setelah dilakukannya pembayaran ke rekening Universitas Bhayangkara Jakarta Raya. Pemesanan formulir hanya dapat dilakukan sekali dalam satu periode gelombang. Apabila pemesan ingin memesan dua formulir maka pemesanan harus dilakukan di periode gelombang yang berbeda. <b><i><u>Mohon simpan bukti pembayaran dengan baik apabila sewaktu-waktu dibutuhkan.</u></i></b>
                                    </div>
                                </div>
                            </div><!--end panel collapse-->            
                        </div>
                    </div>
                </div>
                <form method="post" action="<?php echo base_url('dashboard/booking_form/summary');?>">
                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <center>
                            <input type="checkbox" value="1" name="oke" required/>&nbsp;&nbsp;
                            <input type="hidden" name="user" value="<?php echo $usr['userid']; ?>">
                            <input type="hidden" name="keyy" value="<?php echo $keyy; ?>">
                            <strong>Saya telah membaca dan setuju</strong> dengan syarat dan ketentuan yang berlaku. <br>Saya mengerti dan siap menerima segala konsekuensi yang ada jika tidak memenuhi syarat dan ketentuan berlaku. 
                        </center>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-sm btn-primary">Berikutnya</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


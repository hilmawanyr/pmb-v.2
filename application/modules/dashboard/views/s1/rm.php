<div id="readmisi">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">credit_card</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">NPM <small>(required)</small></label>
                <input name="npm_readmisi" type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">today</i>
            </span>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Tahun Masuk UBJ</label>
                    <input type="text" class="form-control" name="thmasuk_readmisi">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Sampai Dengan Semester</label>
                    <input type="text" class="form-control" id="" name="smtr_readmisi">
                </div>
            </div>
        </div>
    </div>
</div>
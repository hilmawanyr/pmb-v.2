<div id="konversi">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Nama Perguruan Tinggi <small>(required)</small></label>
                <input name="asal_pt" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kategori Perguruan Tinggi <small>(required)</small></label>
                <input type="radio" name="jenis_skl" value="NGR"  required> NEGERI &nbsp;&nbsp;
                <input type="radio" name="jenis_skl" value="SWT" > SWASTA &nbsp;&nbsp; 
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">map</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kota PTN/PTS <small>(required)</small></label>
                <input name="kota_pt" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">local_library</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Program Studi <small>(required)</small></label>
                <input name="prodi_pt" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">today</i>
            </span>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Tahun Lulus</label>
                    <input type="text" class="form-control" name="lulus_pt" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Semester</label>
                    <input type="text" class="form-control" id="" name="smtr_pt" required>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">credit_card</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">NPM/NIM Asal <small>(required)</small></label>
                <input name="npm_pt" type="text" class="form-control" required>
            </div>
        </div>
    </div>
</div>
<div id="new0">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">local_library</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Asal Sekolah <small>(required)</small></label>
                <input name="asal_sch" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">credit_card</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">NISN <small>(required)</small></label>
                <input name="nisn" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">pin_drop</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kota Asal Sekolah <small>(required)</small></label>
                <input name="kota_sch" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">map</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kelurahan Asal Sekolah <small>(required)</small></label>
                <input name="daerah_sch" type="text" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kategori Sekolah <small>(required)</small></label>
                <input type="radio" name="jenis_skl" value="NGR"  required> NEGERI &nbsp;&nbsp;
                <input type="radio" name="jenis_skl" value="SWT" > SWASTA &nbsp;&nbsp; 
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">school</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Jenis Sekolah <small>(required)</small></label>
                <input type="radio" name="jenis_sch_maba" value="SMA"  required> SMA &nbsp;&nbsp;
                <input type="radio" name="jenis_sch_maba" value="SMK" > SMK &nbsp;&nbsp; 
                <input type="radio" name="jenis_sch_maba" value="MDA" > MA  &nbsp;&nbsp;
                <input type="radio" name="jenis_sch_maba" value="SMB" > SMTB  &nbsp;&nbsp;
                <input type="radio" name="jenis_sch_maba" value="OTH" > Lainnya
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">near_me</i>
            </span>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Jurusan</label>
                    <input type="text" class="form-control" name="jur_sch" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group label-floating">
                    <label class="control-label">Tahun Lulus</label>
                    <input type="text" class="form-control" id="" name="lulus_sch" required>
                </div>
            </div>
        </div>
    </div>
</div>
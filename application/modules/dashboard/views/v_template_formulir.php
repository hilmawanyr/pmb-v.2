<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PMB UBJ</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
        <!-- Common plugins -->
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dashboard/plugins/nano-scroll/nanoscroller.css">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/chart-c3/c3.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/blue.css" rel="stylesheet">
        <!-- dataTables -->
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/dashboard/plugins/toast/jquery.toast.min.css" rel="stylesheet">
        <!--template css-->
        <link href="<?php echo base_url(); ?>assets/dashboard/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-109578352-1', 'auto');
          ga('send', 'pageview');
        </script>
    </head>
    <body class="layout-horizontal">

        <!--top bar start-->
        <div class="top-bar bg-primary"><!--by default top bar is dark, add .light-top-bar class to make it light-->
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="index.html" class="admin-logo">
                            <h1><img src="<?php echo base_url(); ?>assets/img/logo.gif" class="" width="40" alt=""> Registrasi Online Ubharajaya</h1>
                        </a>
                    </div>
                    <div class="col-xs-6">
                        <ul class="list-inline top-right-nav">
                            <li class="dropdown avtar-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php $sess = $this->session->userdata('sess_login_pmb'); ?>
                                    <p><?php echo getName($sess['userid']); ?> &nbsp;&nbsp;<i class="fa fa-caret-down"></i></p>
                                </a>
                                <ul class="dropdown-menu top-dropdown">
                                    <li><a href="javascript: void(0);"><i class="icon-settings"></i> Ganti Password</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript: void(0);"><i class="icon-logout"></i> Keluar</a></li>
                                </ul>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
        <!-- top bar end-->

        <!--Main nav start-->
        <!-- Static navbar -->
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="<?php echo base_url('dashboard');?>"><i class="icon-home"></i> Dashboard</a>
                            
                        </li>
                        <li>
                            <a href="<?php echo base_url('dashboard/booking_form');?>"><i class="fa fa-shopping-cart"></i> Pembelian Formulir</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('dashboard/berkas');?>"><i class=" icon-docs"></i> Kelengkapan Berkas</a>
                        </li>
                        <!-- <li>
                            <a href="#"><i class=" icon-user"></i> Data Diri</a>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url('dashboard/guide'); ?>"><i class="fa fa-newspaper-o"></i> Panduan</a>
                        </li>
                    </ul>

                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        <!--MAin nav end-->


        <!--start page content-->

        <div class="h-main-content">
            <div class="container">
                <!-- hanya isi dengan row -->
                <embed height="760" width="1200" src="<?php echo base_url('dashboard/v_formulir/'.$prodi.'/'.$kunci)?>">

                <div class="col-md-12 alert alert-warning alert-dismissible fade in" role="alert">
                    <strong>Jika data anda telah sesuai, anda dapat melanjutkan ke menu <a href="<?php echo base_url('dashboard/berkas') ?>" title="">kelengkapan berkas</a></strong>. 
                    Atau apabila anda ingin merubah data pada formulir diatas, anda dapat <a href="<?php echo base_url('dashboard/editForm/'.$sess['userid'].'/'.$prodi.'/'.$kunci); ?>" title="">klik disini</a>
                </div>
            </div>
            <br><br>
            <!--Start footer-->
            <footer class="footer">
                <div class="container text-center">
                    <span>Copyright &copy; 2016. Float</span>
                </div>
            </footer>
            <!--end footer-->
        </div>

        <!--end page content-->


        <!--Common <?php echo base_url(); ?>assets/dashboard/plugins-->
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/pace/pace.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/nano-scroll/jquery.nanoscroller.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/metisMenu/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/float-custom.js"></script>
        <!--page script-->
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/chart-c3/d3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/chart-c3/c3.min.js"></script>
        <!-- iCheck for radio and checkboxes -->
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/iCheck/icheck.min.js"></script>
        <!-- Datatables-->
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/plugins/toast/jquery.toast.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dashboard/js/dashboard-alpha.js"></script>

    </body>
</html>
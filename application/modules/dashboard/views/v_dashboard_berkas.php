<?php $sess = $this->session->userdata('sess_login_pmb');?>
<script type="text/javascript">
    function loadberkas(path) {
        $('#contentfoto').load('<?= base_url('dashboard/berkas/lihatberkas/') ?>'+path[0]+'/'+path[1]+'/'+path[2]);
    }
    // $(document).ready(function() {
    //     $.ajax({

    //         url: "<?php echo base_url('dashboard/berkas/get_berkas'); ?>",

    //         type: "post",

    //         success: function(d) {

    //             var parse = JSON.parse(d);

    //             if (parse === 'MB') {
    //                 $('.def').show();
    //                 $('#spd').remove();
    //                 $('#str').remove();
    //                 // $('#trk').remove(); 
    //                 $('#baa').remove();
    //                 $('#ren').remove();
    //             // } else if (parse === 'RM') {
    //             //     $('#baa').show();
    //             //     $('#trk').show(); 
    //             //     $('#ren').show();
    //             //     $('#fto').show();
    //             //     $('#akt').show();
    //             //     $('#kk').show();
    //             //     $('#ktp').show();
    //             //     $('#ijz').remove();
    //             //     $('#skh').remove();
    //             //     $('#skl').remove();
    //             //     $('#rpt').remove();
    //             //     $('.kov').remove();
    //             } else if (parse === 'KV') {
    //                 $('#spd').show();
    //                 $('#str').show();
    //                 $('#trk').show();
    //                 $('#fto').show();
    //                 $('#akt').show();
    //                 $('#kk').show();
    //                 $('#ktp').show();
    //                 // $('#ijz').show();
    //                 $('#skh').remove();
    //                 $('#skl').remove();
    //                 $('#rpt').remove();
    //                 $('.rea').remove();
    //             }
    //         }
    //     });
    // });
</script>

<div class="row">
     <div class="col-md-12">
        <div class="panel panel-indigo collapsed">
            <div class="panel-heading">
                KELENGKAPAN BERKAS
            </div>
            <div class="panel-body">
            	
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th>Jenis Berkas</th>
                            <th>Status</th>
                            <th>Validitas</th>
                            <th style="text-align: center">Aksi</th>
                            <th>Lihat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if ($filt->program == 1) {
                                if ($filt->jenis_pmb == 'MB') {
                                    // load berkas sesuai pendaftaran
                                    $coll = $this->crud_model->whereIn('tbl_berkas','id_berkas',array('1','2','3','4','5','6','7','8'),'id_berkas','asc')->result(); 
                                    foreach ($coll as $maba) { ?>
                                        <tr id="<?php echo $maba->kd_berkas; ?>" class="def">
                                            <td style="vertical-align: middle"><?= $maba->berkas; ?></td>
                                            <td style="vertical-align: middle">
                                                <i><u><?php echo complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey); ?></u></i>
                                            </td>
                                            <td>
                                                <?= validHumas($sess['userid'],$maba->id_berkas,$arrkey); ?>
                                            </td>
                                            <td style="text-align: center">
                                                <?php if (complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey) == 'Lengkap') { ?>
                                                    <?php $ulang = $this->crud_model->getMoreWhere('tbl_file',array('userid' => $sess['userid'], 'tipe' => $maba->id_berkas))->row()->valid; ?>
                                                        <?php if ($ulang == 0) { ?>
                                                            <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>        
                                                        <?php } else { ?>
                                                            <i class="fa fa-check"></i>        
                                                        <?php } ?>
                                                    
                                                <?php } else { ?>
                                                    <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-rounded" data-target="#loadfoto" data-toggle="modal" onclick="loadberkas([<?= $sess['userid'] ?>,<?= $maba->id_berkas ?>,<?= $arrkey ?>])"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                <?php    } 
                                } else {
                                    $coll = $this->crud_model->whereIn('tbl_berkas','id_berkas',array('1','2','3','4','7','9','10','11'),'id_berkas','asc')->result(); 
                                    foreach ($coll as $maba) { ?>
                                        <tr id="<?php echo $maba->kd_berkas; ?>" class="def">
                                            <td style="vertical-align: middle"><?= $maba->berkas; ?></td>
                                            <td style="vertical-align: middle">
                                                <i>
                                                    <u>
                                                        <?= complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey); ?>
                                                    </u>
                                                </i>
                                            </td>
                                            <td>
                                                <?= validHumas($sess['userid'],$maba->id_berkas,$arrkey); ?>
                                            </td>
                                            <td style="text-align: center">
                                                <?php if (complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey) == 'Lengkap') { ?>
                                                    <?php $ulang = $this->crud_model->getMoreWhere('tbl_file',array('userid' => $sess['userid'], 'tipe' => $maba->id_berkas))->row()->valid; ?>
                                                        <?php if ($ulang == 0) { ?>
                                                            <a href="#<?= $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>        
                                                        <?php } else { ?>
                                                            <i class="fa fa-check"></i>        
                                                        <?php } ?>
                                                    
                                                <?php } else { ?>
                                                    <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-rounded" data-target="#loadfoto" data-toggle="modal" onclick="loadberkas([<?= $sess['userid'] ?>,<?= $maba->id_berkas ?>,<?= $arrkey ?>])"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                <?php    }
                               }
                            } else {
                                if ($filt->jenis_pmb == 'MB') {
                                    $coll = $this->crud_model->whereIn('tbl_berkas','id_berkas',array('1','2','3','4','7','8','11'),'id_berkas','asc')->result(); 
                                    foreach ($coll as $maba) { ?>
                                        <tr id="<?php echo $maba->kd_berkas; ?>" class="def">
                                            <td style="vertical-align: middle"><?php echo $maba->berkas; ?></td>
                                            <td style="vertical-align: middle"><i><u><?php echo complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey); ?></u></i></td>
                                            <td> <?php echo validHumas($sess['userid'],$maba->id_berkas,$arrkey); ?> </td>
                                            <td style="text-align: center">
                                                <?php if (complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey) == 'Lengkap') { ?>
                                                    <?php $ulang = $this->crud_model->getMoreWhere('tbl_file',array('userid' => $sess['userid'], 'tipe' => $maba->id_berkas))->row()->valid; ?>
                                                        <?php if ($ulang == 0) { ?>
                                                            <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>        
                                                        <?php } else { ?>
                                                            <i class="fa fa-check"></i>        
                                                        <?php } ?>
                                                    
                                                <?php } else { ?>
                                                    <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-rounded" data-target="#loadfoto" data-toggle="modal" onclick="loadberkas([<?= $sess['userid'] ?>,<?= $maba->id_berkas ?>,<?= $arrkey ?>])"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                <?php    } 
                                } else {
                                    $coll = $this->crud_model->whereIn('tbl_berkas','id_berkas',array('1','2','3','7','9','10','11'),'id_berkas','asc')->result(); 
                                    foreach ($coll as $maba) { ?>
                                        <tr id="<?php echo $maba->kd_berkas; ?>" class="def">
                                            <td style="vertical-align: middle"><?php echo $maba->berkas; ?></td>
                                            <td style="vertical-align: middle"><i><u><?php echo complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey); ?></u></i></td>
                                            <td> <?php echo validHumas($sess['userid'],$maba->id_berkas,$arrkey); ?> </td>
                                            <td style="text-align: center">
                                                <?php if (complete('tbl_file','userid',$sess['userid'],'tipe',$maba->id_berkas,'key_booking',$arrkey) == 'Lengkap') { ?>
                                                    <?php $ulang = $this->crud_model->getMoreWhere('tbl_file',array('userid' => $sess['userid'], 'tipe' => $maba->id_berkas))->row()->valid; ?>
                                                        <?php if ($ulang == 0) { ?>
                                                            <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>        
                                                        <?php } else { ?>
                                                            <i class="fa fa-check"></i>        
                                                        <?php } ?>
                                                    
                                                <?php } else { ?>
                                                    <a href="#<?php echo $maba->id_berkas; ?>" data-toggle="modal"><button class="btn btn-info btn-rounded"><i class="fa fa-cloud-upload"></i></button></a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-rounded" data-target="#loadfoto" data-toggle="modal" onclick="loadberkas([<?= $sess['userid'] ?>,<?= $maba->id_berkas ?>,<?= $arrkey ?>])"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                <?php    }
                               }
                            }
                        ?>


                        
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div><!--end row-->

<div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Akta Kelahiran</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="1">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="2" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Kartu Keluarga</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="2">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="3" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">KTP</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="3">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="4" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Ijazah terakhir</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="4">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="5" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">SKHUN</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="5">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="6" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Rapot</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="6">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="7" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Foto (3x4)</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="7">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="8" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Surat Kelulusan</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="8">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="9" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Surat Pindah</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="9">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="10" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Sertifikat Akreditasi</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="10">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="11" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Transkrip</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="11">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="12" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Laporan BAA</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="12">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="13" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Keterangan Renkeu</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/fileIn" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" required="">
                            <i>*Unggah file max. 2 MB</i>
                            <input type="hidden" name="user" value="<?php echo $sess['userid']; ?>">
                            <input type="hidden" name="tipe" value="13">
                            <input type="hidden" name="key" value="<?php echo $this->session->userdata('sess_for_file'); ?>">
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-primary pull-right">Upload</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loadfoto" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Lihat berkas</h3>
            </div>
            <div class="modal-body" id="contentfoto">
                
            </div>
        </div>
    </div>
</div>
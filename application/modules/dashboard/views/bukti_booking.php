<?php

//ob_start();
$pdf = new FPDF("L","mm", "A5");



$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(2, 0 ,0);

$pdf->SetFont('Arial','B',20); 

$pdf->image(site_url().'assets/img/logo.gif',5,2,15);
$pdf->setXY(22,8);
$pdf->Cell(200,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,5,'L');


$pdf->SetLeftMargin(5);
$pdf->setY(22);
$pdf->SetFont('Arial','B',11); 
$pdf->Cell(205,5,'BUKTI PEMESANAN FORMULIR PENERIMAAN MAHASISWA BARU',0,1,'C');


$pdf->setXY(10,37);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,9,'Nama',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,9,getName($load->userid),1,1,'L');

$pdf->setXY(10,46);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Email',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,getEmail($load->userid),1,1,'L');

$pdf->setXY(10,53);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'ID Pemesanan',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,$load->key,1,1,'L');

$pdf->setXY(10,60);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Program Studi',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,get_prodi($load->prodi),1,1,'L');

$pdf->setXY(10,67);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Lokasi Kampus',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,getCamp($load->camp),1,1,'L');

$pdf->setXY(10,74);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Gelombang',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,$load->gel,1,1,'L');

// kondisi program untuk menentukan jumlah tagihan
if ($load->program == 1) {
	$not = 'Rp. 300.000,-';
} else {
	$not = 'Rp. 400.000,-';
}

$pdf->setXY(10,81);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Tagihan',1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,7,$not,1,1,'L');

$pdf->setXY(10,88);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,7,'Nomor Rekening Tujuan',1,0,'L');
$pdf->Cell(140,7,'0424-01-000-7183-02 (BRI)',1,1,'L');


// $pdf->setXY(10,102);
$pdf->ln(9);
$pdf->SetFont('Arial','',9);
$pdf->Cell(5,5,'',0,0,'L');
$pdf->Cell(100,5,'Kartu ini merupakan bukti bahwa pemesan telah melakukan pemesanaan formulir penerimaan mahasiswa baru di Universitas',0,1,'L');
$pdf->Cell(5,5,'',0,0,'L');
$pdf->Cell(100,5,'Bhayangkara Jakarta Raya. Pemesan dapat mencetak kartu ini apabila diperlukan dikemudian hari. Jumlah tagihan yang tertera',0,1,'L');
$pdf->Cell(5,5,'',0,0,'L');
$pdf->Cell(120,5,'pada tabel diatas dapat dibayarkan via transfer ke nomor rekening tujuan atas nama',0,0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(100,5,'Universitas Bhayangkara Jakarta Raya.',0,1,'L');
// $pdf->image(base_url().'assets/img/QRImg/000af148af3fa903fbe98a3f8f21aa67.png',170,110,30);

$pdf->setXY(10,104);
$pdf->Cell(190,15,'',1,1,'C');

$pdf->setXY(80,120);
$pdf->SetFont('Arial','',6);
$pdf->Cell(50,8,'copyright - UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,0,'C');


$pdf->SetFont('Arial','',12);
date_default_timezone_set('Asia/Jakarta'); 

//exit();
$pdf->Output('Bukti_pemesanan_formulir_PMB_UBJ'.date('ymd_his').'.PDF','I');

?>


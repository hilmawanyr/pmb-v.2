<!-- button upload lib -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-filestyle-1.3.0/src/bootstrap-filestyle.min.js">
     $(":file").filestyle({buttonBefore: true});
</script>

<!-- autocomplete for bank -->
<!-- autocomplete lib -->
<link href="<?php echo base_url();?>assets/js/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function($) {

        $('input[name^=bank]').autocomplete({

            source: '<?php echo base_url('dashboard/loadBank');?>',

            minLength: 1,

            select: function (evt, ui) {

                this.form.bank.value = ui.item.value;

            }

        });

    });

</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
    <h3 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h3>
</div>
<div class="modal-body">
    <div class="modal-form">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Via ATM</a></li>
            <li><a data-toggle="tab" href="#menu1">Via Teller</a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <br>
                <form role="form" action="<?php echo base_url(); ?>dashboard/booking_form/payConfirm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" class="form-control" name="no_rek" placeholder="Nomor Rekening" required="">
                        <input type="hidden" name="paytip" value="1">
                        <input type="hidden" value="<?php echo $forkey->key; ?>" name="key">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="nama_rek" placeholder="Rekening Atas Nama" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="bank" id="bank" placeholder="Bank Asal" required="">
                    </div>
                    <div class="form-group">
                        <input type="file" data-btnClass="btn-primary" class="form-control filestyle" placeholder="aaa" name="userfile" required="">
                        <i>*Unggah foto struk (max. 2 MB)</i>
                    </div>
                    <hr>
                    <div class="clearfix">
                        <button type="submit" class="btn  btn-success pull-left">Konfirmasi</button>
                    </div>
                </form>
            </div>
            <div id="menu1" class="tab-pane fade">
                <br>
                <form role="form" action="<?php echo base_url(); ?>dashboard/booking_form/payConfirm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="file" class="filestyle" data-btnClass="btn-primary" name="userfile">
                        <input type="hidden" name="paytip" value="2">
                        <input type="hidden" value="<?php echo $forkey->key; ?>" name="key">
                        <i>*Unggah foto struk (max. 2 MB)</i>
                    </div>
                    <hr>
                    <div class="clearfix">
                        <button type="submit" class="btn btn-success pull-left" data-buttonBefore="true">
                            Konfirmasi
                        </button>
                    </div>
                </form>
            </div>
        </div>
        
        <hr>
    </div>
</div>
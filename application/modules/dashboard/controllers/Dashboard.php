<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Cfpdf');
		if ($this->session->userdata('sess_login_pmb') != TRUE) {
			echo "<script>alert('Menjauh kau penyusup!');</script>";
			redirect(base_url('auth/login/out'),'refresh');
		}
		$this->load->helper('cond_helper');
	}

	public function index()
	{
		error_reporting(0);
		$ses = $this->session->userdata('sess_login_pmb');
		$this->db->where('userid', $ses['userid']);
		$data['load'] = $this->db->get('tbl_booking')->result();

		/* PMB 2018
		$data['load'] = $this->crud_model->getDetail('tbl_booking','userid',$ses['userid'])->result();
		*/
		$arr = array('userid' => $ses['userid'], 'valid !=' => 3);
		$data['roll'] = $this->crud_model->getMoreWhere('tbl_booking',$arr)->result();

		$data['page'] = 'v_dashboard_home';
		$this->load->view('v_template_dashboard',$data);
	}

	function summary()
	{
		$data['page'] = 'v_dashboard_booking_summary';
		$this->load->view('v_template_dashboard',$data);	
	}

	function loadBank()
	{
		$this->db->distinct();

        $this->db->select("bank,kd_bank");

        $this->db->from('tbl_bank');

        $this->db->like('bank', $_GET['term'], 'both');

        $this->db->or_like('kd_bank', $_GET['term'], 'both');

        $load  = $this->db->get();

		$data = array();
		foreach ($load->result() as $value) {
			$data[] = array(
				'bank' => $value->bank,
				'kode' => $value->kd_bank,
				'value' => $value->kd_bank.' - '.$value->bank
			);
		}
		echo json_encode($data);
	}

	function formulir($boo,$key)
	{
		$data['prodi'] = myEncode($boo,FALSE);
		$data['kunci'] = $key;
		$this->load->view('v_template_formulir', $data);	
	}

	function v_formulir($boo,$key)
	{
		$ses = $this->session->userdata('sess_login_pmb');
		$log = $ses['userid'];
		$data['prodi'] = $boo;
		$arr = array('user_input' => $log, 'prodi' => $boo);
		$ray = array('userid' => $log, 'prodi' => $boo);
		$cek = $this->crud_model->getMoreWhere('tbl_form_pmb',$arr)->num_rows();
		if ($cek > 0) {
			$this->load_form($log,$boo);
		} else {
			$data['key'] = $key;
			$data['get'] = $this->crud_model->getMoreWhere('tbl_booking',$ray)->row();
			$this->load->view('v_formulir', $data);	
		}
	}

	function load_form($s,$boo)
	{
		$arr = array('user_input' => $s, 'prodi' => $boo);
		$data['detl'] = $this->crud_model->getMoreWhere('tbl_form_pmb',$arr)->row_array();
		$this->load->view('v_load_detailmaba', $data);
	}

	function compFile()
	{
		$i = $this->session->userdata('sess_login_pmb');
		$p = $this->crud_model->getDetail('tbl_form_pmb','user_input',$i['userid'])->row();
		$a = $this->temph_model->cariselisih($i['userid'],$p->jenis_pmb)->result();
		foreach ($a as $value) {
			echo $value->tipe;
		}
	}

	function passBeforePrint($key)
	{
		if ($this->session->userdata('sess_for_testcard')) {
			$this->session->unset_userdata('sess_for_testcard');
			$this->session->set_userdata('sess_for_testcard', $key);
		} else {
			$this->session->set_userdata('sess_for_testcard', $key);
		}
		
		/* pada PMB 2018 menggunakan fungsi ini
		redirect(base_url('dashboard/printCard'));
		*/

		// PMB v1.0 (2019)
		redirect(base_url('dashboard/cardPrint'));
	}

	function cardPrint()
	{
		$log = $this->session->userdata('sess_login_pmb');
		$key = $this->session->userdata('sess_for_testcard');
		// cek apakah sudah upload foto
		$arr = array('userid' => $log['userid'], 'tipe' => 7);
		$img = $this->crud_model->getMoreWhere('tbl_file',$arr);

		if ($img->num_rows() == 0) {
			echo "<script>alert('ANDA BELUM MENGUNGGAH BERKAS FOTO!');history.go(-1);</script>"; exit();
		}

		$ark = ['user_input' => $log['userid'], 'key_booking' => $key];
		$data['usr'] = $this->crud_model->getMoreWhere('tbl_form_pmb',$ark)->row();
		
		$this->load->library('ciqrcode');
		$params['data'] = $data['usr']->nomor_registrasi.str_replace('.','',$data['usr']->nomor_registrasi);
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.str_replace('.', '', $data['usr']->nomor_registrasi).'.png';
		$this->ciqrcode->generate($params);
		// var_dump($params['savename']);exit();
		$this->load->view('kartu_ujian_pmb', $data);
	}

	function printCard()
	{

		$key = $this->session->userdata('sess_for_testcard');
		
		$log = $this->session->userdata('sess_login_pmb');
		$cek = $this->crud_model->getDetail('tbl_file','userid',$log['userid']);

		// cek sudahkan melengkapi berkas
		$arg 	= array('user_input' => $log['userid'], 'status_form' => 1);
		$file 	= $this->crud_model->getMoreWhere('tbl_form_pmb',$arg)->row();

		// constant for amount of file
		$mbs1 = 7;
		$kvs1 = 8;
		$s2 = 7;

		if ($file->program == '1') {
			if ($file->jenis_pmb == 'MB') {
				if ($cek->num_rows() < 6) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 6) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			} else {
				if ($kvs1 != $cek->num_rows()) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 8) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			}
		} else {
			if ($file->jenis_pmb == 'MB') {
				if ($s2 != $cek->num_rows()) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 7) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			} else {
				if ($s2 != $cek->num_rows()) {
					echo "<script>alert('MOHON LENGKAPI BERKAS-BERKAS PRASYARAT ANDA!');history.go(-1);</script>"; exit();
				} else {
					$jum = $this->db->where('userid',$log['userid'])->where('valid',1)->where('key_booking',$key)->get('tbl_file')->num_rows();
					if ($jum < 7) {
						echo "<script>alert('BEBERAPA BERKAS ANDA BELUM/TIDAK TERVALIDASI!');history.go(-1);</script>"; exit();
					}
				}
			}
		}

		// cek apakah sudah upload foto
		$arr = array('userid' => $log['userid'], 'tipe' => 7);
		$img = $this->crud_model->getMoreWhere('tbl_file',$arr);

		if ($img->num_rows() == 0) {
			echo "<script>alert('ANDA BELUM MENGUNGGAH BERKAS FOTO!');history.go(-1);</script>"; exit();
		}
		$data['usr'] = $this->crud_model->getDetail('tbl_form_pmb','user_input',$log['userid'])->row();
		
		$this->load->library('ciqrcode');
		$params['data'] = $data['usr']->nomor_registrasi.str_replace('.','',$data['usr']->nomor_registrasi);
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'QRImage/'.str_replace('.', '', $data['usr']->nomor_registrasi).'.png';
		$this->ciqrcode->generate($params);
		// var_dump($params['savename']);exit();
		$this->load->view('kartu_ujian_pmb', $data);
	}

	function cekform()
	{
		$log = $this->session->userdata('sess_login_pmb');
		$a = $this->crud_model->getDetail('tbl_form_pmb','user_input',$log['userid'])->num_rows();
		$js = json_encode($a);
		echo $js;

	}

	function postalcode()
	{
		$this->db->distinct();

        $this->db->select("kel,kec,kab,prov,kdpos");

        $this->db->from('tbl_kodepos');

        $this->db->like('kel', $_GET['term'], 'both');

        $this->db->or_like('kdpos', $_GET['term'], 'both');

        $load  = $this->db->get();

		$data = array();
		foreach ($load->result() as $value) {
			$data[] = array(
				'pos' => $value->kdpos,
				'kel' => $value->kel,
				'value' => $value->kdpos.' - '.$value->kel
			);
		}
		echo json_encode($data);
	}

	function editForm($usr,$pro,$key)
	{
		$data['detl'] = $this->crud_model->getMoreWhere('tbl_form_pmb',array('user_input' => $usr, 'prodi' => $pro, 'key_booking' => $key))->row_array();
		$this->load->view('edit_form_pmb', $data);
	}

	function saveFromEdit()
	{
		error_reporting(0);

		extract(PopulateForm());

		$exp = explode('/', $tgl_lahir);
		$fixDate = $exp[2].'-'.$exp[1].'-'.$exp[0];

		// var_dump($fixDate);exit();

		$expos = explode(' - ', $kdpos);
		$pos = $expos[0];

		$data = array(
				'program'				=> $program,
				'nomor_registrasi'		=> $noreg,
				'nik'					=> $nik,
				'jenis_pmb'				=> $jenisdaftar,
				'kampus'				=> $lokasikampus,
				'keterangan'			=> '',
				'kelas'					=> $kelas,
				'nama'					=> strtoupper($nama),
				'kelamin'				=> $jk,
				'status_wn'				=> $wn,
				'kewarganegaraan'		=> strtoupper($wn_txt),
				'tempat_lahir'			=> strtoupper($tpt_lahir),
				'tgl_lahir'				=> $fixDate,
				'agama'					=> $agama,
				'status_nikah'			=> $stsm,
				'status_kerja'			=> $stsb,
				'pekerjaan'				=> strtoupper($stsb_txt),
				'alamat'				=> $rt.','.$rw.','.$lurah.','.$camat,//$alamat,
				'kd_pos'				=> $pos,
				'tlp'					=> $tlp,
				'tlp2'					=> $tlp2,
				'npm_lama_readmisi'		=> $npm_readmisi,
				'tahun_masuk_readmisi'	=> $thmasuk_readmisi,
				'smtr_keluar_readmisi'	=> $smtr_readmisi,
				'asal_sch_maba'			=> $asal_sch,
				'daerah_sch_maba'		=> $daerah_sch,
				'kota_sch_maba'			=> $kota_sch,
				'jenis_sch_maba'		=> $jenis_sch_maba,
				'jur_maba'				=> $jur_sch,
				'lulus_maba'			=> $lulus_sch,
				'asal_pts_konversi'		=> $asal_pt,
				'kota_pts_konversi'		=> $kota_pt,
				'prodi_pts_konversi'	=> $prodi_pt,
				'npm_pts_konversi'		=> $npm_pt,
				'tahun_lulus_konversi'	=> $lulus_pt,
				'smtr_lulus_konversi'	=> $smtr_pt,
				'prodi'					=> $prodi,
				'nm_ayah'				=> strtoupper($nm_ayah),
				'didik_ayah'			=> $didik_ayah,
				'workdad'				=> $workdad,
				'life_statdad'			=> $life_statdad,
				'nm_ibu'				=> strtoupper($nm_ibu),
				'didik_ibu'				=> $didik_ibu,
				'workmom'				=> $workmom,
				'life_statmom'			=> $life_statmom,
				'penghasilan'			=> $gaji,
				'referensi'				=> $refer,
				'tanggal_regis'			=> date('Y-m-d H:i:s'),
				'gelombang'				=> $gelombang,
				'status'				=> '',
				'gelombang_du'			=> '',
				'npm_baru'				=> '',
				'foto'					=> '',
				'status_kelengkapan'	=> '', // $lengkap,
				'status_feeder'			=> '',
				'nisn'					=> $nisn,
				'kategori_skl'			=> $jenis_skl,
				'bpjs'					=> $bpjs,
				'no_bpjs'				=> $nobpjs,
				'user_input'			=> $log,
				'user_renkeu'			=> '',
				'user_baa'				=> '',
				'transport'				=> $transportasi,
				'npm_lama_s2'			=> $npmsatu,
				'th_masuk_s2'			=> $tahunmasuks2,
				'status_form'			=> 1,
				'key_booking'			=> $kunci
			);
		$this->db->where('nomor_registrasi', $noreg);
		$this->db->where('user_input', $log);
		$this->db->where('key_booking', $kunci);
		$this->db->update('tbl_form_pmb', $data);
		echo "<script>alert('Berhasil!');history.go(-2);</script>"; exit();

		// $forBoo = myEncode($prodi, TRUE);
		// redirect(base_url('dashboard/formulir/'.$forBoo.'/'.$kunci));
	}

	function condForStat($typ)
	{
		$sess = $this->session->userdata('sess_login_pmb');
		// hitung jumlah booking, jika lebih dari satu yang divalidasi, maka harus dibatalkan salah satu
        $arr = array('userid' => $sess['userid'], 'valid' => 2);
        $getDet = $this->crud_model->getMoreWhere('tbl_booking',$arr)->num_rows();

        // buat kondisi jika booking > 1
        if ($getDet > 1) {
        	if ($typ == 'L') {
        		echo 'Anda memesan formulir lebih dari satu. Mohon batalkan salah satu pemesanan.';	
        	} elseif ($typ == 'R') {
        		echo '<a href="#voidbooking" data-toggle="modal" class="btn  btn-danger" style="margin:5px;"><i class="fa fa-wrench"></i></a>';
        	}

        // jika boking = 1    
        } else {
            $getJuml = $this->crud_model->getDetail('tbl_file','userid',$sess['userid'])->num_rows();
            $typeReg = $this->crud_model->getDetail('tbl_form_pmb','user_input',$sess['userid'])->row();

            // jika pendaftar S1
            if ($typeReg->program == '1') {
            	// jika jenis pendaftaran = mahasiswa baru
            	if ($typeReg->jenis_pmb == 'MB') {
                    if ($getJuml < 8) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }

                // jika jenis pendaftaran = Readmisi
                } elseif ($typeReg->jenis_pmb == 'RM') {
                    if ($getJuml < 7) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }

                // jika jenis pendaftaran = konversi
                } else {
                    if ($getJuml < 8) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }
                }
            // jika pendatftar S2
            } else {
            	// jika pendaftar = MB
            	if ($typeReg->jenis_pmb == 'MB') {
                    if ($getJuml < 7) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }

                // jika jenis pendaftaran = Readmisi
                } elseif ($typeReg->jenis_pmb == 'RM') {
                    if ($getJuml < 7) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }

                // jika jenis pendaftaran = konversi
                } else {
                    if ($getJuml < 7) {
                    	if ($typ == 'L') {
                    		echo 'Anda belum memenuhi berkas persyaratan. Mohon lengkapi berkas-berkas guna pencetakan kartu tes PMB.';
                    	} elseif ($typ == 'R') {
                    		echo '<a href="'.base_url('dashboard/berkas').'" class="btn  btn-danger" style="margin:5px;">Lengkapi</a>';
                    	}
                        
                    } else {
                    	if ($typ == 'L') {
                    		echo 'Berkas persyaratan sudah terlengkapi!';
                    	} elseif ($typ == 'R') {
                    		echo '<i class="fa fa-check"></i>';
                    	}
                    }
                }
            }
            
        }
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
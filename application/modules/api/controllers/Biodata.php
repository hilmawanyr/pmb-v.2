<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

	private $client_key;

	public function __construct()
	{
		parent::__construct();

		// check app key
		if ($this->input->get_request_header('x-token') != APPKEY) {
			$response = ['status' => 90, 'message' => 'app key not valid'];
			$this->output
				->set_status_header(400)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
			exit();
		}
		$this->client_key = $this->input->get_request_header('client-key');
	}

	public function fetch_new_student_bio()
	{
		// fetch detail maba
		$fetchStudentDetail = $this->db->select('npm_baru, tlp, nm_ayah, nm_ibu, nik, nisn, user_input')
										->from('tbl_form_pmb')
										->where('nomor_registrasi', $this->client_key)
										->get();

		// if detail doesn't exist
		if ($fetchStudentDetail->num_rows() < 1) {
			$response = [
				'status' => 23,
				'message' => 'data is empty'
			];

			$this->output
					->set_status_header(201)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($response))
					->_display();
			exit();
		}
		

		// fetch photo path
		$fetchPhoto = $this->_fetchPhoto($fetchStudentDetail->row()->user_input);

		// fetch email
		$fetchEmail = $this->_fetchEmail($fetchStudentDetail->row()->user_input);

		$studentData = $fetchStudentDetail->row();

		$response = [
			'status' => 1,
			'message' => 'success',
			'data' => [
				'npm' => $studentData->npm_baru,
				'no_hp' => $studentData->tlp,
				'email' => $fetchEmail,
				'nama_ibu' => $studentData->nm_ibu,
				'nama_ayah' => $studentData->nm_ayah,
				'tgl_update' => date('Y-m-d'),
				'ktp' => $studentData->nik,
				'nisn' => $studentData->nisn,
				'image' => 'http://172.16.1.5:802'.$fetchPhoto
			]
		];

		$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response))
				->_display();
		exit();
	}

	/**
	 * fetch new student's photo
	 * @param [string] $studentId [student ID to fetch new student's data]
	 * @return [string] 
	 */
	protected function _fetchPhoto($studentId)
	{
		$fetchPhoto = $this->db->select('path')
								->from('tbl_file')
								->where('userid', $studentId)
								->where('tipe', '7')
								->get()->row()->path;
		return $fetchPhoto;
	}

	/**
	 * fetch new student's email
	 * @param [string] $studentId [student ID to fetch new student's email]
	 * @return [string]
	 */
	protected function _fetchEmail($studentId)
	{
		$fetchEmail = $this->db->select('email')
								->from('tbl_user_login')
								->where('userid', $studentId)
								->get()->row()->email;
		return $fetchEmail;
	}

}

/* End of file Biodata.php */
/* Location: .//tmp/fz3temp-2/Biodata.php */